/*
 * grunt-docline-parser
 * 
 *
 * Copyright (c) 2015 Lorenzo Savini
 * Licensed under the Apache-2.0 license.
 */

'use strict';

module.exports = function(grunt) {

    grunt.registerMultiTask('docline_parser', 'The best Grunt plugin ever.', function() {
        var fs = require('fs');

        var docLineConfig,
            docSourceMap;

        var generalConfigFN = this.config_location.general,
            docMapFN = this.config_location.map;

        if (!generalConfigFN) grunt.fatal("Config_location.general not configured.");
        if (!docMapFN) grunt.fatal("Config_location.map not configured.");

        /*
            Start the process loading the two config JSON files:
                - custom/config.json    =>  Contain the main settings
                - custom/docs/map.json  =>  Contain the doc's recursively nested map

            After these loading operation, it run prepareDOCS

            Please note fs.readFile() asynchronously reads the entire contents of a file
         */
        fs.readFile(generalConfigFN, 'utf8', function(err, data) {
            if (err) {
                grunt.fatal(err);
            }
            docLineConfig = JSON.parse(data);
            fs.readFile(docMapFN, 'utf8', function(err, data) {
                if (err) {
                    grunt.fatal(err);
                }
                docSourceMap = JSON.parse(data);
                prepareDOCS();
            });
        });

        /**
         * prepareDOCS()
         *
         * @brief It prepare the docs build
         *
         * @description It recursively parse docSourceMap and recreate
         * that array adding the checksum of the file.
         *
         * That array is merged with the docLineConfig value and saved in
         *      custom/docs/build/pages.build.json
         *
         * For more optimization, the mergeConfig array contain the
         * checksum of docLineConfig and docSourceMap
         * and the timestamp when this function ran last time.
         *
         * mergeConfig:
         *      - config            =>  The main settings (Docs title and so on), an array not null]
         *      - map               =>  The map of the docs, an array not null
         *          - slug              =>  The final URI fragment of this page
         *          - title             =>  The page's title.
         *          - path              =>  The relative path of the source file;
         *                                  its base path are custom/docs/src/ and custom/src/build/ both
         *          - children          =>  An array of the children pages. A children page may contain
         *                                  a children value itself.
         *          - checksum          =>  The md5 checksum of the doc's source file, calculated by
         *                                  this function
         *      - master_checksum   =>  The md5 checksum of 'config' and 'map'
         *      - timestamp         =>  The timestamp from the UNIX Epoch, taken at the processing
         *                              time of this function
         *
         */
        function prepareDOCS() {
            var mergeConfig = {
                'config': docLineConfig
            };

            // ...

        }
    });

};
