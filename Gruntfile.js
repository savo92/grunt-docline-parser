/*
 * grunt-docline-parser
 * 
 *
 * Copyright (c) 2015 Lorenzo Savini
 * Licensed under the Apache-2.0 license.
 */

'use strict';

module.exports = function(grunt) {
    // load all npm grunt tasks
    require('load-grunt-tasks')(grunt);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        docline_parser: {
            config_location: {
                map: './custom/docs/map.json',
                general: './custom/config.json'
            },
            docs_location: {
                source: './custom/docs/src',
                dest: './custom/docs/build'
            }
        }

    });

    // Actually load this plugin's task(s).
    grunt.loadTasks('tasks');

};
