# grunt-docline-parser

> Plugin developed for DocLine. 
> It parse the doc's source, calculating some useful checksum for optimization.
>
> Basically, it takes a map of files (with hierarchy) and provide the checksum of all the files.

**WIP**: THIS PLUGIN STILL NOT WORK

Normally, this plugin is used with DocLine but you can use this plugin in any other project. 

**IMPORTANT** The development of this plugin was started inside DocLine but it was refactored
to be standalone from DocLine. Until the end of the DocLine's development
this plugin will be updated to work with DocLine. After the end of the development,
we will work more on this

## Getting Started
This plugin requires Grunt.

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```
npm install grunt-docline-parser --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```
grunt.loadNpmTasks('grunt-docline-parser');
```

## The "docline_parser" task

### Overview
In your project's Gruntfile, add a section named `docline_parser` to the data object passed into `grunt.initConfig()`.  


```
grunt.initConfig({  
   docline_parser: {  
       config_location: {    
           map: '',         // where the map.json tile is located 
           general: ''      // where config.json file is located
       },
       docs_location: {
            source: '',     // where the docs source is located
            dest: ''        // where grunt-docline-parser will locate the source after the compiling time
       }
   }  
});
```

### Options

#### config\_location.map
Type: `String`
Default: `./custom/docs/map.json`

#### config\_location.general
Type: `String`
Default: `./custom/config.json`

#### docs\_location.source
Type: `String`
Default: `./custom/docs/src`

#### docs\_location.dest
Type: `String`
Default: `./custom/docs/build`

### Usage Examples

#### Default Options
The configuration below is the default one used in the DocLine's Gruntfile.js. 

```
grunt.initConfig({
    dependencies_injector: {
        config_location: {    
            map: './custom/docs/map.json',    
            general: './custom/config.json'  
        },
        docs_location: {
            source: './custom/docs/src',
            dest: './custom/docs/build'
        }
    },
})
```

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## License
Copyright (c) 2015 Lorenzo Savini. Licensed under the Apache-2.0 license.